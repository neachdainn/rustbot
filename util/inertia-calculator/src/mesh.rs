//! Definitions of inertial primitives.
use nalgebra::geometry::{Point3, Rotation3};
use rustbot::physics::inertia;
use serde_derive::{Deserialize, Serialize};

/// A collection of inertial primitives.
#[derive(Debug, Serialize, Deserialize)]
pub struct Mesh
{
	#[serde(rename = "mesh")]
	shapes: Vec<Primitive>,
}

impl Mesh
{
	/// Calculate the inertial tensor for the mesh.
	pub fn inertial_tensor(&self) -> inertia::Tensor
	{
		// The full tensor is just the sum of the component tensors.
		self.shapes.iter().fold(inertia::Tensor::zeros(), |acc, s| acc + s.inertial_tensor())
	}
}

/// An inertial primitive.
#[derive(Debug, Serialize, Deserialize)]
struct Primitive
{
	/// The shape of the primitive.
	shape: Shape,

	/// The physical properties of the primitive.
	props: Properties,
}

impl Primitive
{
	/// Calculate the inertial tensor for the shape.
	fn inertial_tensor(&self) -> inertia::Tensor
	{
		// Start by getting the "base" shape tensor.
		let base_tensor = self.shape.base_tensor(self.props.mass);

		// Rotation comes after the mass.
		let [roll, pitch, yaw] = self.props.rpy;
		let rot_mat = Rotation3::from_euler_angles(roll, pitch, yaw);
		let rotated_tensor = inertia::rotate_tensor(base_tensor, rot_mat);

		// Finally, apply the parallel axis theorem.
		let [x, y, z] = self.props.offset;
		inertia::parallel_axis(rotated_tensor, self.props.mass, Point3::new(x, y, z))
	}
}

/// Common physical properties of the shapes.
#[derive(Debug, Serialize, Deserialize)]
struct Properties
{
	/// The mass of the object.
	mass: f64,

	/// The offset of the object.
	#[serde(default)]
	offset: [f64; 3],

	/// The rotation of the object.
	#[serde(default)]
	rpy: [f64; 3],
}

/// Common geometric shapes.
#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "type")]
enum Shape
{
	Rod
	{
		size_x: f64
	},

	Plate
	{
		size_y: f64, size_z: f64
	},

	RectPrism
	{
		size_x: f64, size_y: f64, size_z: f64
	},

	Disk
	{
		radius: f64
	},

	Cylinder
	{
		size_x: f64, radius: f64
	},

	Cone
	{
		size_x: f64, radius: f64
	},

	Sphere
	{
		radius: f64
	},
}

impl Shape
{
	/// Get the "base" inertial tensor for the shape.
	///
	/// This is the tensor for the shape with even density and a mass of 1.0, under no
	/// transformations whatsoever.
	fn base_tensor(&self, mass: f64) -> inertia::Tensor
	{
		match *self {
			Shape::Rod { size_x } => inertia::rod(mass, size_x),
			Shape::Plate { size_y, size_z } => inertia::plate(mass, size_y, size_z),
			Shape::RectPrism { size_x, size_y, size_z } => {
				inertia::rect_prism(mass, size_x, size_y, size_z)
			},
			Shape::Disk { radius } => inertia::disk(mass, radius),
			Shape::Cylinder { size_x, radius } => inertia::cylinder(mass, size_x, radius),
			Shape::Cone { size_x, radius } => inertia::cone(mass, size_x, radius),
			Shape::Sphere { radius } => inertia::sphere(mass, radius),
		}
	}
}

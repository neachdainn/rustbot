use std::{fs::File, path::PathBuf};

use easy_error::{ResultExt, Terminator};
use structopt::StructOpt;

mod mesh;

/// Calculates the inertial tensor for the series of primitives described in the YAML file.
#[derive(StructOpt, Debug)]
struct Opt
{
	/// YAML file describing primitives.
	#[structopt(parse(from_os_str))]
	file: PathBuf,

	/// Output the results in a URDF friendly way.
	#[structopt(short = "u", long = "urdf")]
	urdf: bool,
}

/// Entry point of the application.
fn main() -> Result<(), Terminator>
{
	let args = Opt::from_args();

	// First thing we need to do is parse the input file.
	let file = File::open(args.file).context("Unable to open file")?;
	let mesh: mesh::Mesh = serde_yaml::from_reader(&file).context("Failed to parse YAML")?;

	// The output the calculated inertial tensor.
	let tensor = mesh.inertial_tensor();
	if args.urdf {
		println!(
			r#"<inertia ixx="{:e}" ixy="{:e}" ixz="{:e}" iyy="{:e}" iyz="{:e}" izz="{:e}"/>"#,
			tensor[0], tensor[1], tensor[2], tensor[4], tensor[5], tensor[8]
		);
	}
	else {
		println!("{}", tensor);
	}

	Ok(())
}

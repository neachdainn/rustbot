use std::time::{Duration, Instant};

use crate::config::Config;
use easy_error::{err_msg, Result};
use rustbot::na::Point2;
use sfml::{
	graphics::{
		CircleShape,
		Color,
		RenderTarget,
		RenderTexture,
		RenderWindow,
		Shape,
		Sprite,
		Transformable,
	},
	window::{Event, Style},
};

/// Colors used to draw the CPGs.
///
/// Chosen because they were visually distinct, not because they were pretty.
const COLORS: [Color; 6] = [
	Color { r: 239, g: 41, b: 41, a: 255 },
	Color { r: 206, g: 92, b: 0, a: 255 },
	Color { r: 138, g: 226, b: 52, a: 255 },
	Color { r: 114, g: 159, b: 207, a: 255 },
	Color { r: 173, g: 127, b: 168, a: 255 },
	Color { r: 233, g: 185, b: 110, a: 255 },
];

/// A CPG rendering window.
pub struct Window
{
	/// The underlying SFML window.
	window: RenderWindow,

	/// The previously rendered CPG cycles.
	buffer: RenderTexture,

	/// The width of the window.
	width: u32,

	/// The height of the window.
	height: u32,

	/// The size of the line history.
	line_thickness: f32,

	/// The limit of the x-axis.
	x_limit: f32,

	/// The limit of the y-axis.
	y_limit: f32,

	/// The currently active CPG points.
	///
	/// We keep track of these separately so we can draw them to the window instead of the buffer.
	current: Vec<Point2<f32>>,
}

impl Window
{
	/// Create a new window with the specified settings.
	pub fn new(config: &Config, current: Vec<Point2<f32>>) -> Result<Window>
	{
		let (x_limit, y_limit) = (config.x_limit.abs(), config.y_limit.abs());
		let (width, height) = (config.window_width, config.window_height);
		let line_thickness = config.line_thickness.abs();

		// Create the window itself
		let mut window =
			RenderWindow::new((width, height), "CPG Simulator", Style::CLOSE, &Default::default());
		window.set_framerate_limit(config.fps);

		// And then attempt to create the `RenderTexture`.
		let mut buffer = RenderTexture::new(width, height, false)
			.ok_or_else(|| err_msg("Unable to create RenderTexture"))?;

		buffer.clear(Color::WHITE);
		window.clear(Color::WHITE);

		Ok(Window { window, buffer, width, height, line_thickness, x_limit, y_limit, current })
	}

	/// Do nothing but handle events for the specified duration.
	pub fn pause(&mut self, dur: Duration)
	{
		let start = Instant::now();
		while Instant::now().duration_since(start) < dur && self.window.is_open() {
			// The event loop must be processed even if we aren't doing anything.
			while let Some(event) = self.window.poll_event() {
				match event {
					Event::Closed => self.window.close(),
					_ => {},
				}
			}

			// Display a frame just so we continue to obey the maximum FPS
			self.draw();
			self.window.display();
		}
	}

	/// Steps the simulation using the provided points.
	pub fn step(&mut self)
	{
		while let Some(event) = self.window.poll_event() {
			match event {
				Event::Closed => self.window.close(),
				_ => {},
			}
		}

		// Add the current points to the buffer.
		for (&p, &c) in self.current.iter().zip(COLORS.iter()) {
			let p = self.cpg_to_window(p);
			let mut sprite = CircleShape::new(self.line_thickness, 30);
			sprite.set_fill_color(c);
			sprite.set_origin((self.line_thickness, self.line_thickness));
			sprite.set_position((p.x, p.y));
			self.buffer.draw(&sprite);
		}
		self.buffer.display();

		// Draw and display everything.
		self.draw();
		self.window.display();
	}

	/// Returns a reference to the window's current points.
	pub fn points(&self) -> &[Point2<f32>] { &self.current }

	/// Returns a mutable reference to the window's current points.
	pub fn points_mut(&mut self) -> &mut [Point2<f32>] { &mut self.current }

	/// Returns `true` if the window has been closed.
	pub fn is_open(&self) -> bool { self.window.is_open() }

	/// Draws the current state of things to the underlying window.
	fn draw(&mut self)
	{
		// The pipeline is really optimized for clear/draw/display
		self.window.clear(Color::WHITE);

		// Draw the history
		let mut sprite = Sprite::with_texture(self.buffer.texture());
		sprite.set_color(Color { r: 255, g: 255, b: 255, a: 100 });
		self.window.draw(&Sprite::with_texture(self.buffer.texture()));

		// Draw the current points slightly larger. Use a normal loop in order to avoid issues with
		// borrowing.
		for (&p, &c) in self.current.iter().zip(COLORS.iter()) {
			let p = self.cpg_to_window(p);

			// Double draw the circle to give it a black border.
			let r = self.line_thickness * 2.0;
			let mut sprite = CircleShape::new(r, 30);
			sprite.set_fill_color(Color::BLACK);
			sprite.set_origin((r, r));
			sprite.set_position((p.x, p.y));
			self.window.draw(&sprite);

			let r = self.line_thickness * 1.5;
			let mut sprite = CircleShape::new(r, 30);
			sprite.set_fill_color(c);
			sprite.set_origin((r, r));
			sprite.set_position((p.x, p.y));
			self.window.draw(&sprite);
		}
	}

	/// Converts from CPG space to Window space.
	fn cpg_to_window(&self, cpg: Point2<f32>) -> Point2<f32>
	{
		// YEAH technically this is mirrored on the y-axis but that doesn't matter.
		let x = (1.0 + cpg.x / self.x_limit) * (self.width as f32 / 2.0);
		let y = (1.0 + cpg.y / self.y_limit) * (self.height as f32 / 2.0);

		Point2::new(x, y)
	}
}

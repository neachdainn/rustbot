use std::{
	convert::TryInto,
	fs,
	path::PathBuf,
	time::{Duration, Instant},
};

use easy_error::{ensure, ResultExt, Terminator};
use rustbot::{
	cpg::Cpg,
	na::Point2,
};
use structopt::StructOpt;

mod config;
use config::{Config, Point};

mod window;
use window::Window;

/// CPG Simulation utility.
#[derive(Debug, StructOpt)]
struct Opt
{
	/// Config file describing the CPGs to simulate.
	#[structopt(parse(from_os_str))]
	config: Option<PathBuf>,
}

fn main() -> Result<(), Terminator>
{
	// FIXME: This should be exposed as an option or something.
	const LIMIT: f32 = 0.05;

	// Either load and parse the provided config file or just use the defaults.
	let config: Config = Opt::from_args()
		.config
		.map(|p| fs::read_to_string(p))
		.transpose()
		.context("Failed to read config file")?
		.map(|s| toml::from_str(&s))
		.transpose()
		.context("Failed to parse config file")?
		.unwrap_or_default();

	// Eagerly convert all of the CPG specifications into CPG structs in order to report errors
	// early.
	let cpg_params: Vec<(Cpg, Duration)> = config
		.cpg
		.iter()
		.cloned()
		.map(TryInto::try_into)
		.collect::<Result<_, _>>()
		.context("Invalid CPG specification encountered")?;

	// If there are no CPGs to simulate, we might as well just exit.
	if cpg_params.len() < 1 {
		return Ok(());
	}

	// Make sure all of the CPGs are the same size.
	ensure!(
		cpg_params.iter().all(|(c, _)| c.dim() == cpg_params[0].0.dim()),
		"All CPG parameter sets must have the same dimensionality"
	);

	// Now we can generate the initial points. If the user provided them, we need to verify their
	// size.
	let points = match &config.init {
		Some(points) => {
			points.iter()
				.map(|&Point { x, y }| Point2::new(x, y))
				.collect()
		},
		None => cpg_params[0].0.init_points(),
	};

	// Create the window based on the provided configuration
	let mut window = Window::new(&config, points).context("Unable to create window")?;

	// Pause the window to make capturing recordings easier.
	window.pause(Duration::from_micros((config.delay * 1e6) as u64));

	// Then run the CPGs as long as requested.
	let dt = (config.fps as f32).recip();
	for (cpg, dur) in cpg_params {
		println!("{}", cpg);

		if let Some(v) = cpg.is_stable_within(dt, window.points(), (50.0 / dt) as usize, LIMIT) {
			println!("CPG stabilizes in {} seconds", dt * v as f32);
		}

		let mut stable = false;

		let start = Instant::now();
		while window.is_open() && Instant::now().duration_since(start) < dur {
			if let Err(_) = cpg.step(dt, window.points_mut()) {
				break;
			}

			window.step();

			let stable_now = cpg.is_stable(window.points(), LIMIT);
			if !stable && stable_now {
				println!("Stable");
				stable = stable_now;
				window.pause(Duration::from_secs(5));
			}
		}
	}

	// Delay at the end to make analyzing easier.
	window.pause(Duration::from_secs(60 * 60 * 24));

	Ok(())
}

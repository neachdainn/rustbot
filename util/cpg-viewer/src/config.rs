use std::{convert::TryFrom, time::Duration};

use easy_error::{ensure, Error};
use rustbot::{cpg::{Cpg, Shape}, na::DVector};
use serde::Deserialize;

/// A configuration specification for rendering CPGs
#[derive(Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Config
{
	/// Width of the window.
	#[serde(default = "default_window_size")]
	pub window_width: u32,

	/// Height of the window.
	#[serde(default = "default_window_size")]
	pub window_height: u32,

	/// Thickness of the lines.
	#[serde(default = "default_line_thickness")]
	pub line_thickness: f32,

	/// Specifies the maximum update rate of the window.
	#[serde(default = "default_fps")]
	pub fps: u32,

	/// Amount of time to wait before running the simulation.
	#[serde(default = "default_delay")]
	pub delay: f32,

	/// Limit of the x-axis.
	#[serde(default = "default_axis_limit")]
	pub x_limit: f32,

	/// Limit of the y-axis.
	#[serde(default = "default_axis_limit")]
	pub y_limit: f32,

	/// How to initialize the oscillators.
	pub init: Option<Vec<Point>>,

	/// CPGs to render.
	pub cpg: Vec<CpgConfig>,
}

/// Starting points for the CPG.
#[derive(Debug, Deserialize)]
pub struct Point
{
	/// The `x` position.
	pub x: f32,

	/// the `y` position.
	pub y: f32,
}

/// CPG parameters.
#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct CpgConfig
{
	/// Step width.
	a: Vec<f32>,

	/// Step height.
	b: Vec<f32>,

	/// Shape of the limit cycle.
	shape: LimitCycleShape,

	/// Normal velocity modifier.
	gamma: Vec<f32>,

	/// Angular velocity modifier.
	omega: f32,

	/// Coupling matrix.
	k: Vec<f32>,

	/// Running time for the CPG.
	run_time: f32,
}

/// A configuration specification for CPG cycle shape.
#[derive(Clone, Debug, Deserialize)]
pub enum LimitCycleShape
{
	/// A rectangular limit cycle.
	Rectangle,

	/// An ellipsoid limit cycle.
	Ellipse,
}

impl Default for Config
{
	fn default() -> Config
	{
		Config {
			window_width:   default_window_size(),
			window_height:  default_window_size(),
			line_thickness: default_line_thickness(),
			fps:            default_fps(),
			delay:          default_delay(),
			x_limit:        default_axis_limit(),
			y_limit:        default_axis_limit(),
			init:           None,
			cpg:            vec![Default::default()],
		}
	}
}

impl Default for CpgConfig
{
	fn default() -> CpgConfig
	{
		use std::f32::consts::PI;

		CpgConfig {
			a:        vec![0.1; 6],
			b:        vec![0.1; 6],
			shape:    LimitCycleShape::Rectangle,
			gamma:    vec![1.0; 6],
			omega:    1.0,
			k:        vec![1.*PI/3., 2.*PI/3., 3.*PI/3., 4.*PI/3., 5.*PI/ 3.],
			run_time: 5.0 * 60.0,
		}
	}
}

impl TryFrom<CpgConfig> for (Cpg, Duration)
{
	type Error = Error;

	fn try_from(config: CpgConfig) -> Result<Self, Self::Error>
	{
		let CpgConfig { a, b, shape, gamma, omega, k, run_time } = config;

		let a = DVector::from_vec(a);
		let b = DVector::from_vec(b);
		let shape = shape.into();
		let gamma = DVector::from_vec(gamma);
		let k = DVector::from_vec(k);

		ensure!(a.len() > 0, "Must have a non-zero number of oscillators");
		ensure!(a.len() <= Cpg::max_dim(), "Too many oscillators");
		ensure!(a.len() == b.len(), "Incorrect step height dimensionality");
		ensure!(a.len() == gamma.len(), "Incorrect normal velocity dimensionality");
		ensure!(a.len() - 1 == k.len(), "Incorrect coupling size");

		let cpg = Cpg::new(a, b, shape, gamma, omega, k);
		let run_time = Duration::from_micros((run_time * 1e6) as u64);
		Ok((cpg, run_time))
	}
}

impl From<LimitCycleShape> for Shape
{
	fn from(shape: LimitCycleShape) -> Shape
	{
		match shape {
			LimitCycleShape::Rectangle => Shape::Rectangle,
			LimitCycleShape::Ellipse => Shape::Ellipse,
		}
	}
}

/// Provides a default window size.
const fn default_window_size() -> u32 { 800 }

/// Provides a default line thickness.
const fn default_line_thickness() -> f32 { 5.0 }

/// Provides a default update rate.
const fn default_fps() -> u32 { 100 }

/// Provides a default simulation delay.
const fn default_delay() -> f32 { 2.0 }

/// Provides a default axis limit.
const fn default_axis_limit() -> f32 { 0.2 }

//! Rustbot
//!
//! This crate houses Rust libraries and tools that are useful for robotics research.
#![deny(bare_trait_objects, missing_debug_implementations, missing_docs)]
#![deny(clippy::all)]
#![warn(clippy::nursery)]
#![warn(clippy::pedantic)]
#![allow(clippy::use_self)]
#![allow(clippy::must_use_candidate)]

pub use nalgebra as na;

pub mod cpg;
pub mod physics;

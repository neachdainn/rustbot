//! Inertial calculations and primitives.
use nalgebra::geometry::{Point3, Rotation3};
use nalgebra::Vector3;

/// How inertial tensors are represented.
pub type Tensor = nalgebra::Matrix3<f64>;

/// Rotate the tensor by the provided rotation.
pub fn rotate_tensor(base: Tensor, rotation: Rotation3<f64>) -> Tensor
{
	rotation * base * rotation.transpose()
}

/// Apply the parallel axis theorem to the provided tensor.
#[allow(clippy::similar_names)]
pub fn parallel_axis(base: Tensor, mass: f64, new_origin: Point3<f64>) -> Tensor
{
	let (x, y, z) = (new_origin[0], new_origin[1], new_origin[2]);

	// Use `mul_add` to remove a rounding error;
	let ixx = mass * y.mul_add(y, z * z);
	let ixy = mass * (-x * y);
	let ixz = mass * (-x * z);

	let iyx = mass * (-y * x);
	let iyy = mass * z.mul_add(z, x * x);
	let iyz = mass * (-y * z);

	let izx = mass * (-z * x);
	let izy = mass * (-z * y);
	let izz = mass * x.mul_add(x, y * y);

	let shifted_tensor = Tensor::new(ixx, ixy, ixz, iyx, iyy, iyz, izx, izy, izz);

	base + shifted_tensor
}

/// Calculate the inertial tensor for a rod.
///
/// The rod has a length along the x-axis but is infinitesimally small on the y-axis and z-axis. It
/// is centered at the origin.
pub fn rod(mass: f64, size_x: f64) -> Tensor
{
	let iy = (mass / 12.0) * size_x * size_x;
	let iz = iy;

	Tensor::from_diagonal(&Vector3::new(0.0, iy, iz))
}

/// Calculate the inertial tensor for a thin rectangular plate.
///
/// This plate is infinitesimally along the x-axis and is centered at the origin.
pub fn plate(mass: f64, size_y: f64, size_z: f64) -> Tensor
{
	// Use the `mul_add` function to remove a rounding error
	let ix = (mass / 12.0) * size_y.mul_add(size_y, size_z * size_z);
	let iy = (mass / 12.0) * size_z * size_z;
	let iz = (mass / 12.0) * size_y * size_y;

	Tensor::from_diagonal(&Vector3::new(ix, iy, iz))
}

/// Calculate the inertial tensor for a rectangular prism centered at the origin.
pub fn rect_prism(mass: f64, size_x: f64, size_y: f64, size_z: f64) -> Tensor
{
	// Use the `mul_add` function to remove a rounding error.
	let ix = (mass / 12.0) * size_y.mul_add(size_y, size_z * size_z);
	let iy = (mass / 12.0) * size_z.mul_add(size_z, size_x * size_x);
	let iz = (mass / 12.0) * size_x.mul_add(size_x, size_y * size_y);

	Tensor::from_diagonal(&Vector3::new(ix, iy, iz))
}

/// Calculate the inertial tensor for a thin disk.
///
/// This disk is infinitesimally small on the x-axis and otherwise resides in the yz-plane. It is
/// centered at the origin.
pub fn disk(mass: f64, radius: f64) -> Tensor
{
	let ix = (mass / 2.0) * radius * radius;
	let iy = (mass / 4.0) * radius * radius;
	let iz = iy;

	Tensor::from_diagonal(&Vector3::new(ix, iy, iz))
}

/// Calculate the inertial tensor for a circular cylinder centered at the origin.
pub fn cylinder(mass: f64, size_x: f64, radius: f64) -> Tensor
{
	// Use the `mul_add` function to remove a rounding error.
	let ix = (mass / 2.0) * radius * radius;
	let iy = (mass / 12.0) * radius.mul_add(3.0 * radius, size_x * size_x);
	let iz = iy;

	Tensor::from_diagonal(&Vector3::new(ix, iy, iz))
}

/// Returns the inertial tensor for a circular cone with the point at the origin.
pub fn cone(mass: f64, size_x: f64, radius: f64) -> Tensor
{
	// Use the `mul_add` function to remove a rounding error.
	let ix = (3.0 * mass / 10.0) * radius * radius;
	let iy = (3.0 * mass / 5.0) * radius.mul_add(0.25 * radius, size_x * size_x);
	let iz = iy;

	Tensor::from_diagonal(&Vector3::new(ix, iy, iz))
}

/// Calculate the inertial tensor for a sphere centered at the origin.
pub fn sphere(mass: f64, radius: f64) -> Tensor
{
	let ix = (2.0 * mass / 5.0) * radius * radius;

	Tensor::from_diagonal_element(ix)
}

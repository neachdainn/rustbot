//! Implementations of Central Pattern Generators.
use std::{error::Error, fmt, num::NonZeroUsize, ops::Neg};

use nalgebra as na;
use serde::{Deserialize, Serialize};

/// Maximum dimensionality of the CPG implementations.
type D = na::U6;

/// Floating point type used for calculations.
type N = f32;

//////////////////////////////////////////////////////////////////////////////////
// Below are types and values that are "generated" based on the above definitions.
//////////////////////////////////////////////////////////////////////////////////

// Using `#[doc(hidden)]` on public aliases means that the user can still refer to the types but
// changing the alias will not be a breaking change. Essentially, they become anonymous types.

/// Statically sized column vector type.
type VectorN = na::VectorN<N, D>;

/// Statically sized square matrix type.
type MatrixN = na::MatrixN<N, D>;

/// Dynamically sized column vector type.
type DVector = na::DVector<N>;

/// A slice of a vector.
#[doc(hidden)]
pub type VectorSlice<'a> = na::DVectorSlice<'a, N>;

/// An offset-diagonal slice of a matrix.
#[doc(hidden)]
pub type RowSliceTr<'a> = na::MatrixSliceMN<'a, N, na::Dynamic, na::U1, D, na::U1>;

/// A slice of a matrix.
#[doc(hidden)]
pub type MatrixSlice<'a> = na::MatrixSliceN<'a, N, na::Dynamic, na::U1, D>;

/// π in terms of the floating point type.
const PI: N = std::f32::consts::PI;

/// Machine epsilon for the floating point type.
const EPSILON: N = std::f32::EPSILON;

/// Smallest possible positive finite value.
const MIN: N = std::f32::MIN;

/// Largest possible positive finite value.
const MAX: N = std::f32::MAX;

//////////////////////////////////////////////////////////////////////////////////
// End of "generated" types and values
//////////////////////////////////////////////////////////////////////////////////

/// A central pattern generator with constraints applied.
///
/// These constraints make it easier to reason about the stable-state behavior of the CPG.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Cpg
{
	/// The raw, underlying CPG.
	raw: RawCpg,

	/// The value analogous to the rotational velocity.
	constrained_omega: N,

	/// The constrained shape.
	constrained_d: Shape,

	/// The number of limbs visible to the user.
	dim: NonZeroUsize,
}

impl Cpg
{
	/// Creates a new CPG with constrained parameters.
	///
	/// All of the supplied vectors needs to have the same dimensionality except for the `k` vector
	/// which needs to be one fewer. They also need to have a dimensionality less than
	/// `RawCpg::dim`.
	///
	/// # Panics
	///
	/// This function will panic if any of the vectors are the incorrect size.
	pub fn new(a: DVector, b: DVector, shape: Shape, gamma: DVector, omega: N, k: DVector) -> Cpg
	{
		// Store these values for potential later recall.
		let constrained_omega = omega;
		let constrained_d = shape;

		// Must have at least one limb but no more than the max supported number.
		let dim = NonZeroUsize::new(a.len()).expect("Must have at least one oscillator");

		// Make sure all of the dimensionality are correct.
		if a.len() > RawCpg::dim()
			|| b.len() != a.len()
			|| gamma.len() != a.len()
			|| k.len() != a.len() - 1 {
			panic!("Invalid dimensionality found");
		}

		// The step widths, heights, and normal components need to constraining, so they just get
		// turned into static sized vectors.
		let a = dynamic_to_static(&a);
		let b = dynamic_to_static(&b);
		let gamma = dynamic_to_static(&gamma);

		// Because we want the rotational velocity to be the same for all oscillators, the omega
		// components need to be a scaled version of the same value.
		let omega = a.zip_map(&b, |a, b| omega * N::sqrt(a * a + b * b));

		// Calculate the shape vector from the supplied shape.
		let d = VectorN::from_element(shape.value());

		// Create the almost-offset-diagonal matrices for the coupling.
		let k_diag = k;
		let mut k = MatrixN::zeros();
		let mut lambda = MatrixN::zeros();
		for i in 1..dim.get() {
			lambda[(0, i)] = 1.0;
			k[(0, i)] = k_diag[i - 1];
		}

		// The raw CPG values are created.
		let raw = RawCpg { a, b, d, gamma, omega, k, lambda };

		Cpg { raw, constrained_omega, constrained_d, dim }
	}

	/// Performs a single step of the CPG using the given positions.
	///
	/// If multiple steps will happen without needing the intermeidate results, prefer
	/// `Cpg::step_multiple`.
	///
	/// # Panics
	///
	/// If the size of the input is not the same as `Cpg::dim`, this will panic.
	#[inline]
	pub fn step(&self, dt: N, xy: &mut [na::Point2<N>]) -> Result<(), StepError>
	{
		self.step_multiple(1, dt, xy)
	}

	/// Performs multiple CPG steps using the given positions.
	///
	/// # Panics
	///
	/// If the size of the input is not the same as `Cpg::dim`, this will panic.
	pub fn step_multiple(&self, count: usize, dt: N, xy: &mut [na::Point2<N>]) -> Result<(), StepError>
	{
		if xy.len() != self.dim() {
			panic!("Incorrect number of points supplied");
		}

		let mut step = Step::new(xy);
		for _ in 0..count {
			step = self.raw.step(dt, step.x, step.y);
		}

		if step.has_nan() {
			return Err(StepError(step));
		}

		step.put_into(xy);
		Ok(())
	}

	/// Performs a CPG step using the ICRA 2020 formula.
	#[doc(hidden)]
	pub fn step_icra2020(&self, dt: N, xy: &mut [na::Point2<N>]) -> Result<(), StepError>
	{
		if xy.len() != self.dim() {
			panic!("Incorrect number of points supplied");
		}

		let step = Step::new(xy);
		let step = self.raw.step_icra2020(dt, step.x, step.y);
		if step.has_nan() {
			return Err(StepError(step));
		}

		step.put_into(xy);
		Ok(())
	}

	/// Returns the number of oscillators in the CPG.
	pub const fn dim(&self) -> usize { self.dim.get() }

	/// Returns the `a` vector of the CPG.
	pub fn a(&self) -> VectorSlice { VectorSlice::from_slice(self.raw.a.as_slice(), self.dim()) }

	/// Returns the `b` vector of the CPG.
	pub fn b(&self) -> VectorSlice { VectorSlice::from_slice(self.raw.b.as_slice(), self.dim()) }

	/// Returns the `d` vector of the CPG.
	pub fn d(&self) -> VectorSlice { VectorSlice::from_slice(self.raw.b.as_slice(), self.dim()) }

	/// Returns the `gamma` vector of the CPG.
	pub fn gamma(&self) -> VectorSlice
	{
		VectorSlice::from_slice(self.raw.gamma.as_slice(), self.dim())
	}

	/// Returns the `omega` vector of the CPG.
	pub fn omega(&self) -> VectorSlice
	{
		VectorSlice::from_slice(self.raw.omega.as_slice(), self.dim())
	}

	/// Returns the coupling matrix of the CPG.
	pub fn k(&self) -> MatrixSlice
	{
		self.raw.k.slice((0, 0), (self.dim(), self.dim()))
	}

	/// Returns the coupling strength of the CPG.
	pub fn lambda(&self) -> MatrixSlice
	{
		self.raw.lambda.slice((0, 0), (self.dim(), self.dim()))
	}

	/// Returns the shape (constrained `d`) of the CPG.
	pub fn shape(&self) -> Shape { self.constrained_d }

	/// Returns the offsets (constrained coupling matrix) of the CPG transposed to a column vector.
	pub fn offsets_tr(&self) -> RowSliceTr
	{
		// Unfortunately, due to the way the Rust language is (currently?) constructed, this is not
		// possible to do with type aliases.
		RowSliceTr::from_slice_with_strides_generic(
			&self.raw.k.as_slice(),
			na::Dynamic::new(self.dim()),
			na::U1,
			na::U6,
			na::U1,
		)
	}

	/// Creates random initial points near the limit cycles.
	pub fn init_points(&self) -> Vec<na::Point2<N>>
	{
		let (x, y) = self.raw.init_points();
		x.iter().zip(y.iter()).map(|(&x, &y)| na::Point2::new(x, y)).collect()
	}

	/// Returns true if the CPG has reached its stable state, using the given radian limit for
	/// coupling convergence.
	///
	/// # Panics
	///
	/// This function will panic if the size of the input is not the same as `Cpg::dim`.
	pub fn is_stable(&self, xy: &[na::Point2<N>], limit: f32) -> bool
	{
		self.is_stable_within(1e-20, xy, 0, limit).is_some()
	}

	/// Returns true if the CPG has or will reach its stable state within the given time limit,
	/// using the provided radian limit.
	///
	/// # Panics
	///
	/// This function will panic if the size of the input is not the same as `Cpg::dim`.
	pub fn is_stable_within(&self, dt: N, xy: &[na::Point2<N>], steps: usize, limit: f32) -> Option<usize>
	{
		if xy.len() != self.dim() {
			panic!("Incorrect number of points supplied");
		}

		// Use a matrix instead of an array so we can use the `D` type.
		let mut stable = na::VectorN::<bool, D>::from_element(false);
		let stable = &mut stable.as_mut_slice()[0..self.dim()];

		// Convert the radian limit to the sawtooth modulation.
		let limit = (sawtooth_wave(limit) - 1.0).abs();

		// This should probably expose the parameters used in the `nearly_equal` function but that
		// can be addressed later.
		let epsilon = 1e-2;
		let relth = MIN;

		// The mutable states
		let mut step = Step::new(xy);
		let mut limit_cycle_reached = false;

		// Step the CPG until conversion.
		for i in 0..(steps + 1) {
			step = self.raw.step(dt, step.x, step.y);

			if step.has_nan() { return None; }

			if !limit_cycle_reached {
				limit_cycle_reached = step.h.iter().all(|&h| nearly_equal(h, 1.0, epsilon, relth));
			}

			if limit_cycle_reached {
				stable.iter_mut()
					.zip(step.m.iter())
					.for_each(|(s, m)| *s = *s || ((m - 1.0).abs() <= limit));

				if stable.iter().all(|&s| s) {
					return Some(i);
				}
			}
		}

		None
	}

	/// Maximum number of dimensions supported.
	pub fn max_dim() -> usize { RawCpg::dim() }

	/// Returns a reference to the underlying raw CPG.
	pub fn as_raw(&self) -> &RawCpg { &self.raw }

	/// Converts the CPG into its raw form.
	///
	/// Note that this form will have exactly `RawCpg::dim` number of oscillators.
	pub fn into_raw(self) -> RawCpg { self.raw }
}

impl Default for Cpg
{
	fn default() -> Cpg
	{
		let a = DVector::from_vec(vec![0.1; RawCpg::dim()]);
		let b = DVector::from_vec(vec![0.1; RawCpg::dim()]);
		let d = Shape::Rectangle;
		let gamma = DVector::from_vec(vec![1.0; RawCpg::dim()]);
		let omega = 0.25;
		let k = DVector::from_vec(vec![PI; RawCpg::dim() - 1]);

		Cpg::new(a, b, d, gamma, omega, k)
	}
}

impl fmt::Display for Cpg
{
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
	{
		let dim = self.dim();

		write!(f,
			"Cpg(a={:?}, b={:?}, shape={}, gamma={:?}, omega={}, k={:?})",
			&self.raw.a.as_slice()[0..dim],
			&self.raw.b.as_slice()[0..dim],
			self.constrained_d,
			&self.raw.gamma.as_slice()[0..dim],
			self.constrained_omega,
			&self.offsets_tr().transpose().as_slice(),
		)
	}
}


/// A raw central pattern generator.
///
/// This type has zero constraints on it and will always use the maximum number of oscillators.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RawCpg
{
	/// The cycle widths.
	pub a: VectorN,

	/// The cycle heights.
	pub b: VectorN,

	/// The cycle shape.
	pub d: VectorN,

	/// The forcing to the limit cycle (scaling of the normal velocity).
	pub gamma: VectorN,

	/// The base tangential velocity.
	pub omega: VectorN,

	/// The rotational offsets the represents the coupling between the oscillators.
	pub k: MatrixN,

	/// The coupling strength between oscillators.
	pub lambda: MatrixN,
}

impl RawCpg
{
	/// Performs a single step of the CPG using the given positions.
	pub fn step(&self, dt: N, x: VectorN, y: VectorN) -> Step
	{
		///////////////////////////////////////////////////////
		// Components that are used throughout these equations.
		///////////////////////////////////////////////////////
		let xa = x.component_div(&self.a);
		let yb = y.component_div(&self.b);
		let xad = xa.zip_map(&self.d, N::powf);
		let ybd = yb.zip_map(&self.d, N::powf);

		let sign_x = x.map(sign);
		let sign_y = y.map(sign);

		////////////////////////
		// The forcing component
		////////////////////////
		let h = xad + ybd;
		let (dh_x, dh_y) = {
			let x_port = xa.map(N::abs).zip_map(&self.d.map(|d| d - 1.0), N::powf);
			let y_port = yb.map(N::abs).zip_map(&self.d.map(|d| d - 1.0), N::powf);

			let dh_x = self.d.component_mul(&sign_x).component_mul(&x_port).component_div(&self.a);
			let dh_y = self.d.component_mul(&sign_y).component_mul(&y_port).component_div(&self.b);
			(dh_x, dh_y)
		};

		// Normalizing the computed derivatives improves the stability of the limit cycle.
		let (dh_x_norm, dh_y_norm) = {
			let dh_x2 = dh_x.component_mul(&dh_x);
			let dh_y2 = dh_y.component_mul(&dh_y);
			let dh_norm = (dh_x2 + dh_y2).map(N::sqrt);

			(dh_x.component_div(&dh_norm), dh_y.component_div(&dh_norm))
		};

		let forcing_x = self.gamma.zip_map(&h, |g, h| g * (1.0 - h)).component_mul(&dh_x_norm);
		let forcing_y = self.gamma.zip_map(&h, |g, h| g * (1.0 - h)).component_mul(&dh_y_norm);

		////////////////////////////
		// The oscillation component
		////////////////////////////
		let (unit_x, unit_y) = {
			let r = (xad + ybd).zip_map(&self.d.map(N::recip), N::powf);
			let xad_scaled =
				xa.component_div(&r).map(N::abs).zip_map(&self.d.map(|d| d / 2.0), N::powf);
			let ybd_scaled =
				yb.component_div(&r).map(N::abs).zip_map(&self.d.map(|d| d / 2.0), N::powf);

			(sign_x.component_mul(&xad_scaled), sign_y.component_mul(&ybd_scaled))
		};

		// Convert everything to angles
		let angles = unit_y.zip_map(&unit_x, N::atan2);

		// Calculate the errors based on the target angles.
		let errors = {
			// Duplicate the angles vector into columns of a square matrix
			let angles_sq = make_square(angles);

			// Calculate the target angles.
			let targets = angles_sq + self.k;
			targets - angles_sq.transpose()
		};

		// Apply the sawtooth function to the errors. The sawtooth function allows us to treat the
		// number as if it was within the range [-π, π) without having to use a very expensive
		// `fmod` call.
		let modulated = errors
			.map(sawtooth_wave)
			.zip_map(&self.lambda, N::powf)
			.compress_rows_tr(|c| c.iter().product::<N>());

		// Compute the oscillation portion
		let osc_x = self.omega.map(N::neg).component_mul(&dh_y_norm).component_mul(&modulated);
		let osc_y = self.omega.component_mul(&dh_x_norm).component_mul(&modulated);

		////////////////////
		// The full equation
		////////////////////
		let f_x = osc_x + forcing_x;
		let f_y = osc_y + forcing_y;

		let x = x + f_x * dt;
		let y = y + f_y * dt;

		Step { x, y, h, m: modulated }
	}

	/// Performs a single step of the CPG using the given positions and the ICRA 2020 formulation.
	#[doc(hidden)]
	pub fn step_icra2020(&self, dt: N, x: VectorN, y: VectorN) -> Step
	{
		///////////////////////////////////////////////////////
		// Components that are used throughout these equations.
		///////////////////////////////////////////////////////
		let xa = x.component_div(&self.a);
		let yb = y.component_div(&self.b);
		let xad = xa.zip_map(&self.d, N::powf);
		let ybd = yb.zip_map(&self.d, N::powf);

		let sign_x = x.map(sign);
		let sign_y = y.map(sign);

		////////////////////////
		// The forcing component
		////////////////////////
		let h = xad + ybd;
		let (dh_x, dh_y) = {
			let x_port = xa.map(N::abs).zip_map(&self.d.map(|d| d - 1.0), N::powf);
			let y_port = yb.map(N::abs).zip_map(&self.d.map(|d| d - 1.0), N::powf);

			let dh_x = self.d.component_mul(&sign_x).component_mul(&x_port).component_div(&self.a);
			let dh_y = self.d.component_mul(&sign_y).component_mul(&y_port).component_div(&self.b);
			(dh_x, dh_y)
		};

		// Normalizing the computed derivatives improves the stability of the limit cycle.
		let (dh_x_norm, dh_y_norm) = {
			let dh_x2 = dh_x.component_mul(&dh_x);
			let dh_y2 = dh_y.component_mul(&dh_y);
			let dh_norm = (dh_x2 + dh_y2).map(N::sqrt);

			(dh_x.component_div(&dh_norm), dh_y.component_div(&dh_norm))
		};

		let forcing_x = self.gamma.zip_map(&h, |g, h| g * (1.0 - h)).component_mul(&dh_x_norm);
		let forcing_y = self.gamma.zip_map(&h, |g, h| g * (1.0 - h)).component_mul(&dh_y_norm);

		////////////////////////////
		// The oscillation component
		////////////////////////////
		let (f_x, f_y) = {
			let osc_x = self.omega.map(f32::neg).component_mul(&dh_y_norm);
			let osc_y = self.omega.component_mul(&dh_x_norm);

			(osc_x + forcing_x, osc_y + forcing_y)
		};

		/////////////////////////
		// The coupling component
		/////////////////////////
		let (unit_x, unit_y) = {
			let r = h.zip_map(&self.d.map(N::recip), N::powf);
			let xad_scaled =
				xa.component_div(&r).map(N::abs).zip_map(&self.d.map(|d| d / 2.0), N::powf);
			let ybd_scaled =
				yb.component_div(&r).map(N::abs).zip_map(&self.d.map(|d| d / 2.0), N::powf);

			(sign_x.component_mul(&xad_scaled), sign_y.component_mul(&ybd_scaled))
		};

		let (rot_unit_x, rot_unit_y) = {
			// Maybe this should all be done with nalgebra types?
			let cos_k = self.k.map(f32::cos);
			let sin_k = self.k.map(f32::sin);

			let x_sq = make_square(unit_x);
			let y_sq = make_square(unit_y);

			let rot_x = x_sq.component_mul(&cos_k) - y_sq.component_mul(&sin_k);
			let rot_y = x_sq.component_mul(&sin_k) + y_sq.component_mul(&sin_k);
			(rot_x, rot_y)
		};

		let (target_x, target_y) = {
			let a_sq = make_square(self.a).transpose();
			let b_sq = make_square(self.b).transpose();

			let sgn_rot_x = rot_unit_x.map(sign);
			let sgn_rot_y = rot_unit_y.map(sign);

			let two_over_d = make_square(self.d.map(|d| 2.0 / d)).transpose();
			let pow_x = rot_unit_x.map(f32::abs).zip_map(&two_over_d, f32::powf);
			let pow_y = rot_unit_y.map(f32::abs).zip_map(&two_over_d, f32::powf);

			let target_x = a_sq.component_mul(&sgn_rot_x).component_mul(&pow_x);
			let target_y = b_sq.component_mul(&sgn_rot_y).component_mul(&pow_y);
			(target_x, target_y)
		};

		let (err_x, err_y) = {
			let x_sq = make_square(x).transpose();
			let y_sq = make_square(y).transpose();
			let err_x = (target_x - x_sq).component_mul(&self.lambda).row_sum_tr();
			let err_y = (target_y - y_sq).component_mul(&self.lambda).row_sum_tr();

			(err_x, err_y)
		};

		////////////////////
		// The full equation
		////////////////////
		let x = x + (f_x + err_x).map(|dx| dx * dt);
		let y = y + (f_y + err_y).map(|dy| dy * dt);

		Step { x, y, h, m: VectorN::zeros() }
	}

	/// Creates random initial points near the limit cycles.
	pub fn init_points(&self) -> (VectorN, VectorN)
	{
		use rand::{distributions::OpenClosed01, Rng};

		let mut rng = rand::thread_rng();
		let (mut x, mut y) = (VectorN::zeros(), VectorN::zeros());

		for i in 0..Self::dim() {
			let theta = rng.sample::<N, _>(OpenClosed01) * 2.0 * PI;

			let r = (self.a[i] * self.a[i] + self.b[i] * self.b[i]).sqrt();
			let r = rng.sample::<N, _>(OpenClosed01) * r;

			x[i] = r * N::cos(theta);
			y[i] = r * N::sin(theta);
		}

		(x, y)
	}

	/// Returns the number of oscillators used by this implementation.
	pub fn dim() -> usize { <D as na::DimName>::dim() }
}

impl Default for RawCpg
{
	fn default() -> RawCpg
	{
		Cpg::default().into_raw()
	}
}


/// Limit cycle shapes.
#[derive(Clone, Copy, Debug, Deserialize, Serialize)]
pub enum Shape
{
	/// Elongated circle.
	Ellipse,

	/// Rounded rectangle.
	Rectangle,
}

impl Shape
{
	/// Returns the `d` value associated with the shape.
	pub fn value(self) -> N
	{
		match self {
			Shape::Ellipse => 2.0,
			Shape::Rectangle => 4.0,
		}
	}
}

impl fmt::Display for Shape
{
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
	{
		write!(f, "{:?}", self)
	}
}


/// Result of a CPG step that had a `NaN` value.
#[derive(Debug)]
pub struct StepError(Step);

impl fmt::Display for StepError
{
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
	{
		write!(f, "NaN encountered in CPG step")
	}
}

impl Error for StepError
{
	fn description(&self) -> &str { "NaN encountered in CPG step" }
}


/// The result of a raw step.
#[derive(Debug)]
pub struct Step
{
	/// The new x-values.
	pub x: VectorN,

	/// The new y-values.
	pub y: VectorN,

	/// The super ellipse levels.
	pub h: VectorN,

	/// The modulation values.
	pub m: VectorN,
}

impl Step
{
	/// Creates an initial step using a slice of points.
	fn new(xy: &[na::Point2<N>]) -> Step
	{
		let (mut x, mut y) = (VectorN::zeros(), VectorN::zeros());
		x.iter_mut()
			.zip(y.iter_mut())
			.zip(xy.iter().cycle())
			.for_each(|((x, y), p)| { *x = p.x; *y = p.y; });

		Step { x, y, h: VectorN::zeros(), m: VectorN::zeros() }
	}

	/// Places the step into the point slice.
	fn put_into(&self, xy: &mut [na::Point2<N>])
	{
		xy.iter_mut()
			.zip(self.x.iter())
			.zip(self.y.iter())
			.for_each(|((p, &x), &y)| { p.x = x; p.y = y; });
	}

	/// Returns true if the step contains a NaN value.
	fn has_nan(&self) -> bool
	{
		// We only need to check x and y values because any NaN in the other two will have
		// "infected" the x or y value.
		self.x.iter().copied().any(N::is_nan) || self.y.iter().copied().any(N::is_nan)
	}
}


/// Resizes a dynamically sized array into a fixed sized one.
#[inline]
fn dynamic_to_static(d: &DVector) -> VectorN
{
	let mut s = VectorN::zeros();

	let iter = d.as_slice().iter().copied().cycle();
	s.iter_mut().zip(iter).for_each(|(s, d)| *s = d);

	s
}

/// The mathematical `sign` function.
#[inline]
fn sign(x: N) -> N
{
	if x == 0.0 { 0.0 } else { x.signum() }
}

/// Implementation of the sawtooth wave.
#[inline]
fn sawtooth_wave(x: N) -> N
{
	let tau = PI * 2.0;
	2.0 * ((x / tau) - N::floor(0.5 + (x / tau))) + 1.0
}

/// Duplicates the column vector to make a square matrix.
fn make_square(v: VectorN) -> MatrixN
{
	let mut sq = MatrixN::zeros();
	for i in 0..RawCpg::dim() {
		for j in 0..RawCpg::dim() {
			sq[(i, j)] = v[i];
		}
	}

	sq
}

/// Compares two floating point values to determine if they are nearly equal.
///
/// # Panics
///
/// The epsilon cannot be less than `N::EPSILON` and cannot be more than one.
// Taken from: https://stackoverflow.com/a/32334103
fn nearly_equal(a: N, b: N, epsilon: N, relth: N) -> bool
{
	assert!(EPSILON <= epsilon);
	assert!(epsilon < 1.0);

	if a == b { return true; }

	let diff = (a - b).abs();
	let norm = (a.abs() + b.abs()).min(MAX);
	let res = diff < relth.max(epsilon * norm);

	res
}


#[cfg(test)]
mod test
{
	use super::*;

	#[test]
	fn constrained_coupling_matrices()
	{
		// Values that don't really matter for this test.
		let a = DVector::from_vec(vec![1.; 4]);
		let b = a.clone();
		let shape = Shape::Rectangle;
		let gamma = a.clone();
		let omega = 1.0;

		// The actual value that matters.
		let k_in = DVector::from_vec(vec![1., 2., 3.]);
		let cpg = Cpg::new(a, b, shape, gamma, omega, k_in);
		let k_out = cpg.as_raw().k;
		let lambda_out = cpg.as_raw().lambda;

		#[rustfmt::ignore]
		let expected_k = MatrixN::new(
			0.0, 1.0, 2.0, 3.0, 0.0, 0.0,
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
		);

		#[rustfmt::ignore]
		let expected_lambda = MatrixN::new(
			0.0, 1.0, 1.0, 1.0, 0.0, 0.0,
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
		);

		assert_eq!(k_out, expected_k);
		assert_eq!(lambda_out, expected_lambda);
	}

	#[test]
	fn constrained_offset_slice()
	{
		// Values that don't really matter for this test.
		let a = DVector::from_vec(vec![1.; 4]);
		let b = a.clone();
		let shape = Shape::Rectangle;
		let gamma = a.clone();
		let omega = 1.0;

		// The actual value that matters.
		let k_in = DVector::from_vec(vec![1., 2., 3.]);
		let cpg = Cpg::new(a, b, shape, gamma, omega, k_in.clone());
		let k_out = cpg.offsets_tr().into();

		assert_eq!(k_in.insert_row(0, 0.0), k_out);
	}
}

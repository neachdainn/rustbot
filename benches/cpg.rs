use std::time::Instant;

use criterion::{BatchSize, black_box, Criterion, criterion_group, criterion_main};
use rustbot::cpg::Cpg;

pub fn default_cpg_step_single(c: &mut Criterion)
{
	let cpg = Cpg::default();
	c.bench_function("CPG Single Step (default, Δt=0.01)", |b| b.iter_batched_ref(
		|| cpg.init_points(),
		|points| cpg.step(0.01, points),
		BatchSize::SmallInput,
	));
}

pub fn default_cpg_icra_step_single(c: &mut Criterion)
{
	let cpg = Cpg::default();
	c.bench_function("CPG ICRA2020 Single Step (default, Δt=0.01)", |b| b.iter_batched_ref(
		|| cpg.init_points(),
		|points| cpg.step_icra2020(0.01, points),
		BatchSize::SmallInput,
	));
}

pub fn default_cpg_step_multiple(c: &mut Criterion)
{
	let cpg = Cpg::default();
	c.bench_function("CPG Multiple Step (default, Δt=0.01)", |b| b.iter_custom(
		|iters| {
			let mut points = cpg.init_points();
			let start = Instant::now();
			let _ = cpg.step_multiple(iters as usize, 0.01, &mut points);
			let elapsed = start.elapsed();
			black_box(points);

			elapsed
		}
	));
}

criterion_group!(benches,
	default_cpg_step_single,
	default_cpg_icra_step_single,
	default_cpg_step_multiple
);
criterion_main!(benches);
